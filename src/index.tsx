import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

export const GlobalSettingsContext = React.createContext({showDetails : false,
setShowDetails : (details : boolean) => {}});

ReactDOM.render(
  <React.StrictMode>
   <App />
  </React.StrictMode>,
  document.getElementById('root')
);


