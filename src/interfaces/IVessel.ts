import { IVesselDetails  } from "./IVesselDetails";

export interface IVessel {
   
        ID : number,
        Name : string,
        ChristianName: string,
        ImagePath: string,
        Details: IVesselDetails
}

export interface IVesselList {
    vessel : IVessel,
    onDeleteVessel : (id: number) => void
}

