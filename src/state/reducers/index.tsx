import { combineReducers } from "redux";
import { vesselDataReducer } from "./vesselDataReducer";

export default combineReducers({
    vesselDataState : vesselDataReducer
})