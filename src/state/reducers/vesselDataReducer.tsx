import { IVessel } from '../../interfaces/IVessel';
import {UPDATE_DATA, DELETE_DATA} from '../actionTypes';

const initialState = {
    vesselData: []
}

type IACtion  = {
    type : string,
    payload: [] | number
}

export const vesselDataReducer = (state = initialState, action : IACtion) => {
    switch(action.type) {
        case UPDATE_DATA : {
            return {
                ...state,
                vesselData: action.payload
            }
        }
        case DELETE_DATA : {
            const { payload } = action;
            const updatedData = state.vesselData.filter((i: IVessel) => i.ID !== payload);
            return {
                ...state,
                vesselData : updatedData
            }
        }
        default : return {
            ...state
        }
    }

}