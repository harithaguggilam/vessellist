import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import getVesselDataApi from '../../services/vesselDataService';
import { UPDATE_DATA,DELETE_DATA } from '../actionTypes';

export const fetchDataThunk = () => {
    return async (dispatch : ThunkDispatch<{}, {}, AnyAction>) => {
        try {
            const result = await getVesselDataApi();
            const action = { type: UPDATE_DATA, payload: result };
            dispatch(action);
        } catch (err) {
            console.error(err);
        }
    };
};

export const onDeleteVessel = (vesselId: number) => {
    return {
            type: DELETE_DATA,
            payload: vesselId
    }
  }
