import { useStore } from "react-redux"
import VesselItemComponent from "./VesselItemComponent";
import { IVessel } from "../interfaces/IVessel";

export default function VesselListComponent() {
    // Access data via Redux `store`.
    const store = useStore();
    const { vesselDataState : { vesselData } } = store.getState();

   return (
        <div className="vesselItems">
                {vesselData.map((vessel: IVessel) => {
                    return <VesselItemComponent vessel = {vessel}  />;
                })}
            </div>
   )
   
}

