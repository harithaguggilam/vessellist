import { useContext } from "react";
import '../styles/vesselItemstyle.scss';
import { Container, Row, Col,  ListGroup, Card, Button } from 'react-bootstrap';
import { GlobalSettingsContext } from "..";
import { IVesselList } from "../interfaces/IVessel";
import { connect } from "react-redux";
import { onDeleteVessel } from "../state/actions/vesselDataAction";

function VesselItemComponent(props: IVesselList)  {

    // Access single item data from props.
    const { showDetails } = useContext(GlobalSettingsContext)
    const { vessel, onDeleteVessel } = props;

    const DeleteVessel = () => {
            onDeleteVessel(vessel.ID)
    }

    // Access the `GlobalSettingsContext` to know if you should show details or not.

    return (
        <Card body>
            <Container fluid="sm">
            <Row>
            <Col>
            <img src={vessel.ImagePath} alt="Not Available" style={{width:"130px"}} />
          </Col>
          <Col>
              <div>
             <ListGroup>
                <ListGroup.Item>{vessel.Name}</ListGroup.Item>
                <ListGroup.Item>{vessel.ChristianName}</ListGroup.Item>
            </ListGroup>
            </div>
          </Col>
          <Col>
            <Button variant="danger" onClick={DeleteVessel}>Delete Vessel </Button>
          </Col>
          </Row>
          {showDetails && (
          <Row>
                <Col>{vessel.Details.IMONumber} - {vessel.Details.Type}</Col>
            <Col></Col>
            <Col></Col>
          </Row>
          )}
            </Container>
      </Card>
                          
    );
}

const mapDispatchToProps = {
    onDeleteVessel
}

export default connect(null, mapDispatchToProps)(VesselItemComponent)