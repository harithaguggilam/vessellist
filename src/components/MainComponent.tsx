import { useContext, useEffect, useState } from "react";
import { connect } from "react-redux";
import {Button, Container, Row,Col, Card} from 'react-bootstrap'
import { GlobalSettingsContext } from "..";
import VesselListComponent from "./VesselListComponent";
import {fetchDataThunk } from '../state/actions/vesselDataAction';

function MainComponent(props: any) {
    const { showDetails, setShowDetails } = useContext(GlobalSettingsContext);
    const { fetchDataThunk,  vesselData } = props;
    const [ isDataLoaded, setDataLoaded ] = useState(false)
    
    useEffect(() => {
        fetchDataThunk();
    }, [])

    useEffect(() => {
        setDataLoaded(true)
    }, [vesselData])

    const toggleDetails = () => {
        setShowDetails(!showDetails);
    };

    return (
            <div className="main-component">
                <div className="main-header">
                    <Card body > 
                    <Container fluid="sm">
                        <Row>
                            <Col>
                    <h1>Vessel List</h1>
                    </Col>
                    <Col></Col>
                    <Col>
                    <Button variant="info" onClick={toggleDetails}>Show Details</Button>
                    </Col>
                    </Row>
                    </Container>
                    </Card>
                </div>
                {isDataLoaded && <VesselListComponent />}
            </div>
        );
    }
const mapDispatchToProps = {
    fetchDataThunk
}

function mapStateToProps(state: any) {
    const { vesselData } = state.vesselDataState
    return vesselData
}

export default connect(mapStateToProps, mapDispatchToProps)(MainComponent);