const mockData = [
    { ID: 1, Name: 'TT SEAWISE GIANT', ChristianName: 'Gargantula', 
    ImagePath : './images/Vessel1.jpg',
        Details: { Type: 'Tanker', IMONumber: 7381154 } },
    { ID: 2, Name: 'EVER GIVEN', ChristianName: 'Querdenker',
    ImagePath : './images/Vessel2.jpg',
    Details: { Type: 'Cargo', IMONumber: 9811000 } },
    { ID: 3, Name: 'HMM ALGECIRAS', ChristianName: 'Thicc Boi',
    ImagePath : './images/Vessel3.jpg',
    Details: { Type: 'Cargo', IMONumber: 9863297 } },
];

export default function getVesselDataApi() {
    return new Promise(resolve => {
        window.setTimeout(() => resolve(mockData), 100);
    });
}

