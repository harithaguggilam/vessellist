import { useState } from 'react';
import { applyMiddleware, createStore } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import './App.css';
import { GlobalSettingsContext } from '.';
import rootReducer from './state/reducers';
import MainComponent from './components/MainComponent';

export const store = createStore(rootReducer, {}, applyMiddleware(thunk));

function App() {
  const [ showDetails, setShowDetails ] = useState(false);
  const value = { showDetails, setShowDetails};
  
  return (
    <div className="App">
      <Provider store={store}>
      <GlobalSettingsContext.Provider value={value}>
        <MainComponent />
      </GlobalSettingsContext.Provider>
    </Provider>
    </div>
  );
}

export default App;
